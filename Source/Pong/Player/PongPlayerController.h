// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"

#include <Game/PongGameModeBase.h>
#include <Game/Menu/GameMenu.h>

#include "PongPlayerController.generated.h"

/**
 *
 */
UCLASS()
class PONG_API APongPlayerController : public APlayerController
{
  GENERATED_BODY()

public:
  UFUNCTION(BlueprintImplementableEvent, BlueprintCallable, Category = "gameState")
    void newGame();
  UFUNCTION(BlueprintImplementableEvent, BlueprintCallable, Category = "gameState")
    void restartGame();
  UFUNCTION(BlueprintImplementableEvent, BlueprintCallable, Category = "gameState")
    void pauseGame();
  UFUNCTION(BlueprintImplementableEvent, BlueprintCallable, Category = "gameState")
    void resumeGame();
  UFUNCTION(BlueprintImplementableEvent, BlueprintCallable, Category = "gameState")
    void closeToTitleGame();
  UFUNCTION(BlueprintImplementableEvent, BlueprintCallable, Category = "gameState")
    void quitGame();


protected:
  UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "references", meta = (BlueprintProtected))
    APongGameModeBase* gameModeRef;
  UPROPERTY(BlueprintReadWrite, Category = "references", meta = (BlueprintProtected))
    UGameMenu* gameMenuRef;
};
