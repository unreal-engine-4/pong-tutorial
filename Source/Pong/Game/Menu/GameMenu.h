// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "Button.h"

//circular dependency
class APongPlayerController;

#include "GameMenu.generated.h"

/**
 *
 */
UCLASS()
class PONG_API UGameMenu : public UUserWidget
{
  GENERATED_BODY()

public:
  UFUNCTION(BlueprintImplementableEvent, BlueprintCallable, Category = "game")
    void startGame();
  UFUNCTION(BlueprintImplementableEvent, BlueprintCallable, Category = "game")
    void resumeGame();
  UFUNCTION(BlueprintImplementableEvent, BlueprintCallable, Category = "game")
    void restartGame();
  UFUNCTION(BlueprintImplementableEvent, BlueprintCallable, Category = "game")
    void endMatch();
  UFUNCTION(BlueprintImplementableEvent, BlueprintCallable, Category = "game")
    void exitGame();

  UFUNCTION(BlueprintImplementableEvent, BlueprintCallable, Category = "layout")
    void setUpLayout();

  UFUNCTION(BlueprintImplementableEvent, BlueprintCallable, Category = "options")
    void changeOptions();

protected:
  UPROPERTY(BlueprintReadWrite, Category = "references", meta = (BlueprintProtected))
    APongPlayerController* playerControlerRef;

  UFUNCTION(BlueprintCallable, Category = "layout")
    UButton* getNextFocusableButton(TArray<UButton*> buttons);

  UFUNCTION(BlueprintCallable, Category = "layout")
    UButton* getPrevFocusableButton(TArray<UButton*> buttons);

private:
  UButton* findNextFocusableButton(TArray<UButton*> buttons, bool reverse);
};
