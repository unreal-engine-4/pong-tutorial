// Fill out your copyright notice in the Description page of Project Settings.

#include "GameMenu.h"

UButton * UGameMenu::getNextFocusableButton(TArray<UButton*> buttons)
{
  UButton* nextButton = this->findNextFocusableButton(buttons, false);

  if (!nextButton) {
    nextButton = *buttons.FindByPredicate(
      [](const UButton* button) { return button->IsVisible() && button->IsFocusable; }
    );
  }

  return nextButton;
}

UButton * UGameMenu::getPrevFocusableButton(TArray<UButton*> buttons)
{
  UButton* nextButton = this->findNextFocusableButton(buttons, true);

  if (!nextButton) {
    for (int i = buttons.Num() - 1; i >= 0 && !nextButton; --i) {
      UButton* const button = buttons[i];
      if (button->IsVisible() && button->IsFocusable) {
        nextButton = button;
      }
    }
  }

  return nextButton;
}

UButton* UGameMenu::findNextFocusableButton(TArray<UButton*> buttons, bool reverse) {
  int focusedButtonIndex = -1;
  UButton* nextButton = nullptr;

  const int begin = reverse ? buttons.Num() - 1 : 0;
  const int end = reverse ? -1 : buttons.Num();
  const int it = reverse ? -1 : 1;

  for (int i = begin; i != end && !nextButton; i += it) {
    UButton* const button = buttons[i];
    if (button->HasUserFocus((APlayerController*)this->playerControlerRef)) {
      focusedButtonIndex = i;

    }

    if (focusedButtonIndex != -1 && i != focusedButtonIndex && button->IsVisible() && button->IsFocusable) {
      nextButton = button;
    }
  }

  return nextButton;
}