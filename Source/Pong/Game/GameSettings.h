// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/SaveGame.h"
#include "GameSettings.generated.h"

/**
 *
 */
UCLASS()
class PONG_API UGameSettings : public USaveGame
{
  GENERATED_BODY()

public:
  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "paddle")
    bool isPlayerOnLeftSide = true;
  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "paddle")
    float paddleMoveSpeed = 800.0f;
  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "paddle")
    FVector leftPaddleStartPosition = FVector(-850.0, 10.0, 0.0);
  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "paddle")
    FVector rightPaddleStartPosition = FVector(850.0, 10.0, 0.0);

  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ball")
    FVector ballStartPosition = FVector(0.0, 10.0, 0.0);
  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ball")
    float ballSpeed = 1400.0f;

  UFUNCTION(BlueprintCallable, Category = "utilities")
    bool valueEquals(const UGameSettings* value);
};
