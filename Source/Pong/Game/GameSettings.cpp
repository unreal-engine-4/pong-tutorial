// Fill out your copyright notice in the Description page of Project Settings.

#include "GameSettings.h"

bool UGameSettings::valueEquals(const UGameSettings* value)
{
  return this->ballSpeed == value->ballSpeed
    && this->paddleMoveSpeed == value->paddleMoveSpeed
    && this->isPlayerOnLeftSide == value->isPlayerOnLeftSide
    && this->leftPaddleStartPosition.Equals(value->leftPaddleStartPosition)
    && this->rightPaddleStartPosition.Equals(value->rightPaddleStartPosition)
    && this->ballStartPosition.Equals(value->ballStartPosition);
}
