// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"

//https://google.github.io/styleguide/cppguide.html#Forward_Declarations
#include <Gameplay/Ball.h>
#include <Gameplay/Paddle.h>
#include <Game/HUD/GameHUD.h>
#include <Game/GameSettings.h>

#include "PongGameModeBase.generated.h"

/**
 *
 */
UCLASS()
class PONG_API APongGameModeBase : public AGameModeBase
{
  GENERATED_BODY()

public:
  UFUNCTION(BlueprintImplementableEvent, BlueprintCallable, Category = "score")
    void increaseLeftPaddleScore();
  UFUNCTION(BlueprintImplementableEvent, BlueprintCallable, Category = "score")
    void increaseRightPaddleScore();
  UFUNCTION(BlueprintImplementableEvent, BlueprintCallable, Category = "ball")
    void spawnNewBall();
  UFUNCTION(BlueprintImplementableEvent, BlueprintCallable, Category = "paddle")
    void replaceAIWithPlayerPaddle();
  UFUNCTION(BlueprintImplementableEvent, BlueprintCallable, Category = "ball")
    void respawnBall();
  UFUNCTION(BlueprintImplementableEvent, BlueprintCallable, Category = "logic")
    void restartAIMatch();

  UFUNCTION(BlueprintImplementableEvent, BlueprintCallable, Category = "logic")
    void startNewGame();

  UPROPERTY(BlueprintReadWrite, Category = "references")
    ABall* ballRef;

  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "settings")
    UGameSettings* gameSettings;

  UFUNCTION(BlueprintImplementableEvent, BlueprintCallable, Category = "settings")
    void loadSettings();
  UFUNCTION(BlueprintImplementableEvent, BlueprintCallable, Category = "settings")
    void updateSettings(UGameSettings* settings);
  UFUNCTION(BlueprintImplementableEvent, BlueprintCallable, Category = "settings")
    void testSettings(UGameSettings* settings);
  UFUNCTION(BlueprintImplementableEvent, BlueprintCallable, Category = "settings")
    void reloadSettings();

protected:
  UFUNCTION(BlueprintImplementableEvent, BlueprintCallable, Category = "paddle", meta = (BlueprintProtected))
    void spawnBothAIPaddles();

  UPROPERTY(BlueprintReadWrite, Category = "references", meta = (BlueprintProtected))
    UGameHUD* gameHUDRef;
  UPROPERTY(BlueprintReadWrite, Category = "references", meta = (BlueprintProtected))
    APaddle* leftPaddleRef;
  UPROPERTY(BlueprintReadWrite, Category = "references", meta = (BlueprintProtected))
    APaddle* rightPaddleRef;

  UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "ball", meta = (BlueprintProtected))
    float direction = -1;

  UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "score", meta = (BlueprintProtected))
    int leftPaddleScore = 0;
  UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "score", meta = (BlueprintProtected))
    int rightPaddleScore = 0;

  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "settings", meta = (BlueprintProtected))
    FString gameSettingsSlot = "gameSettings";
};
