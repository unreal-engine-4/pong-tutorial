// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "PaperSpriteComponent.h"
#include "Components/SphereComponent.h"
#include "Math/UnrealMathUtility.h"

//https://google.github.io/styleguide/cppguide.html#Forward_Declarations
#include <Gameplay/Paddle.h>


#include "Ball.generated.h"

UCLASS()
class PONG_API ABall : public AActor
{
  GENERATED_BODY()

public:
  // Sets default values for this actor's properties
  ABall();

  UFUNCTION(BlueprintCallable, Category = "movement")
    void updateVelocity(const FVector& velocity);
  UFUNCTION(BlueprintCallable, Category = "movement")
    void updateSpeed(const float& speed);

protected:
  UFUNCTION(BlueprintCallable, Category = "hit response", meta = (BlueprintProtected))
    void bounceOffPaddle(APaddle* paddle);
  UFUNCTION(BlueprintCallable, Category = "hit response", meta = (BlueprintProtected))
    void bounceOffBoundary();

  UFUNCTION(BlueprintCallable, Category = "movement", meta = (BlueprintProtected))
    float getActualSpeed();

private:
  UPROPERTY(VisibleAnyWhere, BlueprintReadOnly, Category = "sprite", meta = (AllowPrivateAccess = "true"))
    UPaperSpriteComponent* sprite;

  UPROPERTY(VisibleDefaultsOnly, BlueprintReadOnly, Category = "collision", meta = (AllowPrivateAccess = "true"))
    USphereComponent* sphere;

  float paddleBounceRangeLimit = 200.0f;

  FVector getPaddleBounceVelocity(APaddle* paddle);
  float getPaddleBounceVelocityZ(APaddle* paddle);
  float getPaddleBounceVelocityX(const float& newVelocityZ);
};
