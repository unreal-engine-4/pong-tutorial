// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Components/BoxComponent.h"

//https://google.github.io/styleguide/cppguide.html#Forward_Declarations
#include <Game/PongGameModeBase.h>

#include "Boundary.generated.h"


UCLASS()
class PONG_API ABoundary : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ABoundary();

protected:
  UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "boundary type", meta = (BlueprintProtected))
  bool isGoal = false;

  UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "boundary type", meta = (BlueprintProtected))
  bool isLeftGoal = false;

  UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "references", meta = (BlueprintProtected))
  APongGameModeBase* gameModeRef;

private:
  UPROPERTY(VisibleAnywhere, Category = "collision")
  UBoxComponent* bounds;
	
	
};
