// Fill out your copyright notice in the Description page of Project Settings.

#include "Ball.h"


ABall::ABall()
{
  PrimaryActorTick.bCanEverTick = false;

  sphere = CreateDefaultSubobject<USphereComponent>(TEXT("collision"));
  RootComponent = sphere;

  sprite = CreateDefaultSubobject<UPaperSpriteComponent>(TEXT("sprite"));
  sprite->SetupAttachment(RootComponent);
}


void ABall::updateVelocity(const FVector& velocity)
{
  this->sphere->SetPhysicsLinearVelocity(velocity);
}

void ABall::updateSpeed(const float& speed)
{
  const float actualSpeed = this->getActualSpeed();
  const float ratio = speed / actualSpeed;
  const FVector velocity = this->sphere->GetPhysicsLinearVelocity();
  this->sphere->SetPhysicsLinearVelocity(velocity * ratio);
}

void ABall::bounceOffPaddle(APaddle * paddle)
{
  FVector velocity = this->getPaddleBounceVelocity(paddle);
  this->sphere->SetPhysicsLinearVelocity(velocity);
}

void ABall::bounceOffBoundary()
{
  FVector velocity = this->sphere->GetPhysicsLinearVelocity();
  velocity.Z *= -1;
  this->sphere->SetPhysicsLinearVelocity(velocity);
}

FVector ABall::getPaddleBounceVelocity(APaddle * paddle)
{
  const float velocityZ = this->getPaddleBounceVelocityZ(paddle);
  const float velocityX = this->getPaddleBounceVelocityX(velocityZ);

  return FVector(velocityX, this->sphere->GetPhysicsLinearVelocity().Y, velocityZ);
}

float ABall::getPaddleBounceVelocityZ(APaddle * paddle)
{
  const FVector paddleVelocity = paddle->getLinearVelocity();
  float velocityZ = this->sphere->GetPhysicsLinearVelocity().Z;

  if (!velocityZ) {
    velocityZ = this->getActualSpeed() * 0.2f;
  }

  if (FMath::GridSnap(paddleVelocity.Z, 100.0f)) {
    const float bounceRangeLimit = velocityZ > 0 ? this->paddleBounceRangeLimit : this->paddleBounceRangeLimit * -1;

    if (FMath::Sign(paddleVelocity.Z) * FMath::Sign(velocityZ) == 1) {
      return FMath::RandRange(velocityZ, velocityZ + bounceRangeLimit);
    }

    return FMath::RandRange(velocityZ - bounceRangeLimit, velocityZ);
  }

  return FMath::RandRange(velocityZ * 0.85f, velocityZ * 1.15f);
}

float ABall::getPaddleBounceVelocityX(const float & newVelocityZ)
{
  const FVector linearVelocity = this->sphere->GetPhysicsLinearVelocity();
  const float speed = FMath::Pow(linearVelocity.X, 2) + FMath::Pow(linearVelocity.Z, 2);

  float newVelocityX = FMath::Sqrt(FMath::Abs(speed - FMath::Pow(newVelocityZ, 2)));
  if (linearVelocity.X > 0) {
    newVelocityX *= -1;
  }

  return newVelocityX;
}

float ABall::getActualSpeed() {
  const FVector linearVelocity = this->sphere->GetPhysicsLinearVelocity();
  return FMath::Sqrt(FMath::Pow(linearVelocity.X, 2) + FMath::Pow(linearVelocity.Z, 2));
}