// Fill out your copyright notice in the Description page of Project Settings.

#include "Paddle.h"


// Sets default values
APaddle::APaddle()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

  collider = CreateDefaultSubobject<UBoxComponent>(TEXT("collision"));
  RootComponent = collider;

  paddleSprite = CreateDefaultSubobject<UPaperSpriteComponent>(TEXT("sprite"));
  paddleSprite->SetupAttachment(RootComponent);
}

void APaddle::updateSpeed(const float & speed)
{
  this->moveSpeed = speed;
}

FVector APaddle::getLinearVelocity()
{
  return this->collider->GetPhysicsLinearVelocity();
}
