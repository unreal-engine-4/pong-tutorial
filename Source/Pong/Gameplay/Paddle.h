// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "PaperSpriteComponent.h"
#include "Components/BoxComponent.h"

class ABall;

#include "Paddle.generated.h"

UCLASS()
class PONG_API APaddle : public APawn
{
  GENERATED_BODY()

public:
  APaddle();


  UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "sprite")
    UPaperSpriteComponent* paddleSprite;

  UFUNCTION(BlueprintCallable, Category = "movement")
    void updateSpeed(const float& speed);
  UFUNCTION(BlueprintCallable, Category = "movement")
    FVector getLinearVelocity();

protected:
  UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "collision", meta = (BlueprintProtected))
    UBoxComponent* collider;

  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "movement", meta = (BlueprintProtected))
    float moveSpeed;

  UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "reference", meta = (BlueprintProtected))
    ABall* ballRef;
};
